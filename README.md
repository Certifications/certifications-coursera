# coursera-certificates

List and Details  

- Course "Internet History, Technology, and Security"  
  by University of Michigan on Coursera.  
  Verified Certificate earned on May 25th, 2015. Grade 99.3%
- Course "R Programming"  
  by Johns Hopkins University on Coursera.  
  Statement of Accomplishment with Distinction earned on June 5th, 2015. Grade 100%
- Course "Introduction to Programming with MATLAB"  
  by Vanderbilt University on Coursera.  
  Statement of Accomplishment earned on June 24th, 2015. Grade 95.7%
- Course "An Introduction to Interactive Programming in Python (Part 1)"  
  by Rice University on Coursera.  
  Verified Certificate with Distinction earned on July 13th, 2015. Grade 98.7%
- Course "Assembling Genomes and Sequencing Antibiotics (Bioinformatics II)"  
  by University of California, San Diego on Coursera.  
  Statement of Accomplishment with Distinction earned on July 20th, 2015. Grade 96.8%
- Course "Finding Hidden Messages in DNA (Bioinformatics I)"  
  by University of California, San Diego on Coursera.  
  Statement of Accomplishment with Distinction earned on July 21st, 2015. Grade 95.3%
- Course "History of Rock, Part Two"  
  by University of Rochester on Coursera.  
  Verified Certificate earned on July 8th, 2015. Grade 100%
- Course "New Models of Business in Society"  
  by University of Virginia on Coursera.  
  Verified Certificate earned on May 25th, 2015. Grade 100%
- Course "Computer Science 101"  
  by Stanford University (this course used to be offered through Coursera, now it is offered through Stanford Online).  
  Statement of Accomplishment earned on August 25th, 2015. Grade 100%.
- Course "Synapses, Neurons and Brains"  
  by Hebrew University of Jerusalem on Coursera.  
  Verified Certificate earned on August 30th, 2015. Grade 95.3%.
- Course "Model Thinking"  
  by University of Michigan on Coursera.  
  Statement of Accomplishment with Distinction earned on September 3rd, 2015. Grade 93.9%.
- Course "Programming for Everybody (Python)"  
  by University of Michigan on Coursera.  
  Statement of Accomplishment with Distinction earned on September 3rd, 2015. Grade 100%.
- Course "Programming for Everybody (Getting Started with Python)"  
  by University of Michigan on Coursera.  
  Verified Certificate earned on September 15th, 2015. Grade 100%.
- Course "Python Data Structures"  
  by University of Michigan on Coursera.  
  Verified Certificate earned on September 15th, 2015. Grade 100%.
- Course "Comparing Genes, Proteins, and Genomes (Bioinformatics III)"  
  by University of California, San Diego on Coursera.  
  Statement of Accomplishment with Distinction earned on September 17th, 2015. Grade 92.3%
- Course "Cryptography I"  
  by Stanford University on Coursera.  
  Statement of Accomplishment with Distinction earned on October 15th, 2015. Grade 100%
- Course "An Introduction to Interactive Programming in Python (Part 2)"  
  by Rice University on Coursera.  
  Verified Certificate with Distinction earned on October 16th, 2015. Grade 100%
- Course "Principles of Computing (Part 1)"  
  by Rice University on Coursera.  
  Verified Certificate with Distinction earned on October 16th, 2015. Grade 97.6%
- Course "Using Python to Access Web Data"  
  by University of Michigan on Coursera.  
  Verified Certificate earned on October 27th, 2015. Grade 97.7%
- Course "Machine Learning"  
  by Stanford University on Coursera.  
  Verified Certificate earned on November 8th, 2015. Grade 98.0%
- Course "Mining Massive Datasets"  
  by Stanford University on Coursera.  
  Statement of Accomplishment earned on November 16th, 2015. Grade 78.8%
- Course "Principles of Computing (Part 2)"  
  by Rice University on Coursera.  
  Verified Certificate with Distinction earned on November 19th, 2015. Grade 99.0%
- Course "Algorithms: Design and Analysis, Part 1"  
  by Stanford University on Coursera.  
  Statement of Accomplishment earned on December 8th, 2015. Grade 100%
- Course "Using Databases with Python"  
  by University of Michigan on Coursera.  
  Verified Certificate earned on December 15th, 2015. Grade 98.9%
- Course "Introduction to Big Data"  
  by University of California, San Diego on Coursera.  
  Verified Certificate earned on December 21st, 2015. Grade 100%
- Course "Machine Learning Foundations: A Case Study Approach"  
  by University of Washington on Coursera.  
  Verified Certificate earned on December 26th, 2015. Grade 99%
- Course "Hadoop Platform and Application Framework"  
  by University of California, San Diego on Coursera.  
  Verified Certificate earned on December 28th, 2015. Grade 100%
- Course "Audio Signal Processing for Music Applications"  
  by Stanford University and UPF on Coursera.  
  Statement of Accomplishment earned on January 9th, 2016. Grade 86.7%
- Course "Capstone: Retrieving, Processing, and Visualizing Data with Python"  
  by University of Michigan on Coursera.  
  Verified Certificate earned on April 5th, 2016. Grade 100%
- Course "Cluster Analysis in Data Mining"  
  by University of Illinois at Urbana-Champaign on Coursera.
  Verified Certificate earned on December 23rd, 2016. Grade 97.5%
- Course "Pattern Discovery in Data Mining"  
  by University of Illinois at Urbana-Champaign on Coursera.
  Verified Certificate earned on December 30th, 2016. Grade 93%
- Specialization "Python for Everybody" Certificate
  by University of Michigan on Coursera.  
  Verified Certificate earned on April 5th, 2016.
